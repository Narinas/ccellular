/*
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <etrujillom1600@alumno.ipn.mx> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return. Eder A. Trujillo
 * Montaño 
 */

#define CELL '0'


#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>

typedef struct _BOARD_{
    int x;
    int y;
    int n;
    int ** current;
    int ** previous;
}BOARD;

void initb(BOARD * board, int x, int y);
void freeb(BOARD * board);
void printb(BOARD board);
void nextg(BOARD * board);
void read_game(FILE * fp, BOARD * board);
int is_alive(BOARD board, int x, int y);
void print_info(BOARD board);

const int neigh[8][2]={
    {0,1},{1,1},{1,0},{1,-1}, \
    {0,-1},{-1,-1},{-1,0},{-1,1}};

int main(int argc, char **argv){
    int i, j;
    char c;
    FILE * file;
    BOARD board;
    if(argc != 2)
        return 1;
    file= fopen(argv[1], "r");
    read_game(file, &board);
    initscr(); cbreak(); noecho();
    clear();
    while(c!='d'){
        printb(board);
        print_info(board);
        refresh();
        c=getch();
        clear();
        nextg(&board);
    }
    fclose(file);
    freeb(&board);
	return 0;
}	
void initb(BOARD * board, int x, int y){
    int i, j;
    board->x=x;
    board->y=y;
    board->n=0;
    board->current = malloc(x*sizeof(int*));
    board->previous = malloc(x*sizeof(int*));
    for(i=0; i<x; i++){
        (board->current)[i] = malloc(y*sizeof(int));
        (board->previous)[i] = malloc(y*sizeof(int));
    }
    for(i=0; i<x ; i++){
        for(j=0; j<y ; j++){
            (board->current)[i][j]=0;
            (board->previous)[i][j]=0;
        }
    }
}
void freeb(BOARD * board){
    int i, j;
    for(i=0; i<board->x; i++){
        free((board->current)[i]);
        free((board->previous)[i]);
    }
    free(board->current);
    free(board->previous);

}
void printb(BOARD board){
    int i, j;
    for(i=0;i<board.x;i++){
        for(j=0;j<board.y;j++){
            mvprintw(j+3, i+3, "%c",(board.current)[i][j]?'0':' ');
        }
    }
}
void nextg(BOARD * board){
    int i, j;
    (board->n)++;
    for(i=0;i<board->x;i++){
        for(j=0;j<board->y;j++){
            (board->current)[i][j]=is_alive(*board, i, j);
        }
    }
    for(i=0;i<board->x;i++){
        for(j=0;j<board->y;j++){
            (board->previous)[i][j]=(board->current)[i][j];

        }
    }
}
int is_alive(BOARD board, int x, int y){
    int sum=0;
    int i;
    for(i=0;i<8;i++){
            sum = sum + board.previous[((((x+neigh[i][0])%board.x)+board.x)%board.x)]\
                  [((((y+neigh[i][1])%board.y)+board.y)%board.y)];
        }
    if(sum==3||(sum==2 && board.previous[x][y]==1))
        return 1;
    else
        return 0;
}
void read_game(FILE * fp, BOARD * board){
    int i,j;
    int x=0; int y=1;
    char c;
    while(fgetc(fp)!='\n'){
        x++;
    }
    while((c=fgetc(fp))!=EOF){
        if(c=='\n')y++; 
    }
    initb(board,x,y);
    rewind(fp);
    for(j=0;j<board->y;j++){
        for(i=0;i<board->x;i++){
            if((c=fgetc(fp))=='\n')
                c=fgetc(fp);
            (board->current)[i][j]=(board->previous)[i][j]=c=='1'?1:0;
        }
    }
}

void print_info(BOARD board){
    mvprintw(0,20,"x: %d y: %d, gen: %d ", board.x, \
            board.y, board.n);

}





